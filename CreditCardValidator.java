import java.util.Scanner;
public class CreditCardValidator {
	
	//Main Method
	public static void main(String[] args){
		//membuat scanner dan meng-assign ke variabel
		Scanner input = new Scanner(System.in);
		System.out.print("Masukkan nomor pada kartu kredit sebagai sebuah bilangan bulat long: \n");
		long noCC = (long)input.nextLong();
		if (isValid(noCC) == true)
			System.out.print("Nomor Kartu " +noCC+""+ " valid.");
		else
			System.out.print("Nomor Kartu " +noCC+""+ " tidak valid.");
	}

	/** Mengembalikan nilai true jika nomor pada kartu kredit valid **/
	public static boolean isValid(long number){
		return (getSize(number)>= 13 && getSize(number)<=16 && prefixValid(number) && sumOfOddAndEven(number));
	}
	
	/** Mengembalikan hasil dari Langkah 2 **/
	public static int sumOfDoubleEvenPlace(long number){
		int jumlah = 0;
		String noCCString = number + "";
		for (int i = getSize(number)-2; i>= 0; i-=2)
			jumlah += getDigit(Integer.parseInt(noCCString.charAt(i)+"")*2);
		return jumlah;
	}
	
	/** Mengembalikan bilangan number jika hanya terdiri dari satu digit; jika tidak, kembalikan hasil jumlah kedua digitnya **/
	public static int getDigit(int number){
		if (number < 9)
			return number;
		else
			return number/10 + number%10;
	}
	
	/** Mengembalikan hasil jumlah digit-digit pada posisi ganjil pada number **/
	public static int sumOfOddPlace(long number){
		int jumlah = 0;
		String noCCString = number + "";
		for (int i = getSize(number)-1; i>= 0; i-=2)
			jumlah += Integer.parseInt(noCCString.charAt(i)+"");
		return jumlah;
	}

	/** Mengembalikan nilai true jika digit-digit d adalah prefiks / awalan dari bilangan number **/
	public static boolean prefixMatched(long number, int d){
		if (d<10)
			return getPrefix(number, 1) == d;
		else
			return getPrefix(number, 2) == d;
	}

	/** Mengembalikan banyak digit-digit pada d **/
	public static int getSize(long d){
		String number = d + "";
		return number.length();
	}
	
	/** Mengembalikan k digit pertama dari number. Jika banyak digitnya kurang dari k, kembalikan nilai number. **/
	public static long getPrefix(long number, int k){
		String noCCString = number + "";
		if (k>getSize(number))
			return number;
		else{
			if (k == 1) 
			return (Long.parseLong(noCCString.substring(0,1)));
			else
				return(Long.parseLong(noCCString.substring(0,2)));
		}
	}
	/** Mengembalikan nilai true ketika salah satu prefixMatched yang sesuai dengan ketentuan bernilai benar **/
	public static boolean prefixValid(Long number){
		return (prefixMatched(number,4)|| prefixMatched(number, 5)||prefixMatched(number, 6)||prefixMatched(number, 37));
	}
	/** Mengembalikan nilai boolean sesuai dengan ketentuan jumlah sumofoddPlace dan sumofDoubleEvenPlace yang dimodulo 10  **/
	public static boolean sumOfOddAndEven(Long number){
		int totalGanjilGenap = sumOfOddPlace(number) + sumOfDoubleEvenPlace(number);
		return totalGanjilGenap%10 == 0;
	}
}